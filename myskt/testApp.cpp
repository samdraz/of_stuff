#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

    gui = new ofxUICanvas(0.0,0.0,200,300);
    gui->setFont("GUI/NewMedia Fett.ttf");
    gui->addToggle("FULL",false);
    gui->autoSizeToFitWidgets();
    ofAddListener(gui->newGUIEvent, this, &testApp::guiEvent);
    gui->loadSettings("setting.xml");

    sf.loadSound("7.mp3");
}

//--------------------------------------------------------------
void testApp::update(){
    ofSoundUpdate();
}

//--------------------------------------------------------------
void testApp::draw(){
    ofDrawBitmapString("Welcome",400,100);


}
void testApp::exit(){
    gui->saveSettings("setting.xml");
    delete gui;
}
void testApp::guiEvent(ofxUIEventArgs &e){
    if(e.widget->getName() == "FULL"){
        ofxUIToggle *tg = (ofxUIToggle *) e.widget;
        ofSetFullscreen(tg->getValue());
    }
}
//--------------------------------------------------------------
void testApp::keyPressed(int key){
    if(key == OF_KEY_F3)
    {
        sf.play();
        ofLogVerbose()<<"playing";
    }

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}
